# Używamy oficjalnego obrazu Pythona jako podstawy
FROM python:3.12-slim

# Ustalamy katalog roboczy w kontenerze
WORKDIR /app

# Instalujemy pakiety systemowe wymagane do budowy bibliotek
RUN apt-get update && \
    apt-get install -y \
    gcc \
    pkg-config \
    libmariadb-dev \
    && rm -rf /var/lib/apt/lists/*

# Instalujemy wymagane pakiety Python
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Kopiujemy kod źródłowy do kontenera
COPY . .

# Otwieramy port 8080 dla aplikacji
EXPOSE 8080

# Ustawiamy zmienną środowiskową Django
ENV DJANGO_SETTINGS_MODULE=world.settings

# Komenda uruchamiająca aplikację
CMD ["gunicorn", "--bind", "0.0.0.0:8080", "world.wsgi"]
