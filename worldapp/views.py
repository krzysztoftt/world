from django.shortcuts import render
from django.views.generic import ListView
from worldapp.models import *

class WorldListView(ListView):
	model = Country
	queryset = Country.objects.order_by('name')

	def get_context_data(self,**kwargs):
		context = super(WorldListView, self).get_context_data(**kwargs)
		context['abc_menu'] = [chr(x) for x in range(65,91)]
		return context

class World_abcListView(ListView):
	model = Country
	
	def get_queryset(self):
		q = self.kwargs['word']
		return Country.objects.filter(name__istartswith= q).order_by('name')

	def get_context_data(self, **kwargs):
	    context = super(World_abcListView, self).get_context_data(**kwargs)
	    context['abc_menu'] = [chr(x) for x in range(65,91)]
	    return context

class CountryListView(ListView):
	model = Country
	template_name = 'worldapp/country.html'
	allow_empty = False

	def get_queryset(self):
		country = self.kwargs['country'].replace("-"," ")
		return Country.objects.filter(name=country)

	def get_context_data(self, **kwargs):
	    context = super(CountryListView, self).get_context_data(**kwargs)
	    context['abc_menu'] = [chr(x) for x in range(65,91)]
	    context['country'] = self.kwargs['country']
	    context['country_obj'] = Country.objects.get(name=self.kwargs['country'].replace("-"," "))
	    return context