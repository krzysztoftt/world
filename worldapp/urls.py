from django.urls import path , re_path
from worldapp.views import *

app_name = 'worldapp'
urlpatterns = [
 path('world/',WorldListView.as_view(),name='world'),   
 re_path(r'^world/(?P<word>\w)/$',World_abcListView.as_view(), name='world'),
 path('world/<slug:country>/',CountryListView.as_view(),name='country'),
]